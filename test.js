const util = require('util');
const wait = util.promisify(setTimeout);

async function test() {
    return Promise.all([[["a", "b", "c"], ["c", "b", "a"], ["b", "a", "c"]]].map(async (groups) => {        
        for (let group of groups) {
            console.log(`doing group ${group}`);
            for (let e of group) {
                console.log(`iterating element ${e}`);
                await wait(100);
            }
            console.log("doing wait");
            await wait(200);
        }
    }));
}  

(async () => { 	await test(); })();
