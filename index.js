
// https://dev.to/sonyarianto/practical-puppeteer-how-to-upload-a-file-programatically-4nm4

const puppeteer = require("puppeteer");

(async () => {
    let downloadUrl = await uploadFile("package.json");
    console.log(downloadUrl);
})();

async function uploadFile(fileToUpload) {
     // set some options (set headless to false so we can see 
    // this automated browsing experience)
    let launchOptions = { headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox']  };

    const browser = await puppeteer.launch(launchOptions);
    const page = await browser.newPage();

    // set viewport and user agent (just in case for nice viewing)
    await page.setViewport({width: 1366, height: 768});
    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

    // go to the target web
    await page.goto('https://tmp.ninja/');
    await page.addScriptTag({url: 'https://code.jquery.com/jquery-3.2.1.min.js'});

    // get the selector input type=file (for upload file)
    await page.waitForSelector('input[type=file]');

    // get the ElementHandle of the selector above
    const inputUploadHandle = await page.$('input[type=file]');

    // Sets the value of the file input to fileToUpload
    inputUploadHandle.uploadFile(fileToUpload);

    // doing click on button to trigger upload file
    await page.waitForSelector('#upload-btn');
    await page.evaluate(() => document.getElementById('upload-btn').click());

    // wait for selector that contains the uploaded file URL
    await page.waitForSelector('.file-url');

    // get the download URL
    let downloadUrl = await page.evaluate(() => {
        return $('.file-url')[0].innerText;
    });

    // close the browser
    await browser.close();

    return `https://${downloadUrl}`;
}
