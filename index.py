import requests

files = {
    'files[]': ('package.json', open('package.json', 'rb')),
}

# https://docs.python-requests.org/en/latest/user/quickstart/#post-a-multipart-encoded-file
response = requests.post('https://tmp.ninja/upload.php', files=files)
print(response.json());
